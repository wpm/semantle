from fastapi.testclient import TestClient
from pytest import fixture

from semantle import __version__
from semantle.server import app


def test_health_check_on_root(api_client):
    response = api_client.get("/")
    assert response.status_code == 200
    assert response.json() == {"API": "Semantle Embeddings", "version": __version__}


@fixture
def api_client():
    # https://fastapi.tiangolo.com/advanced/testing-dependencies/
    return TestClient(app)
