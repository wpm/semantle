from traceback import print_tb

from pytest import fixture
from typer.testing import CliRunner, Result

from semantle import __version__
from semantle.command import app


@fixture
def runner():
    return CliRunner()


def test_help(runner: CliRunner):
    result = runner.invoke(app, ["--help"])
    assert_command_success(result)


def test_version(runner: CliRunner):
    result = runner.invoke(app, ["--version"])
    assert_command_success(result)
    assert __version__ in result.output


def assert_command_success(result: Result):
    # noinspection PyTypeChecker
    assert (
        result.exit_code == 0
    ), f"\n{result.output}\n\n{result.exception}\n{print_tb(result.exc_info[2])}\n"
