"""
Machine assistant for the game Semantle
"""
import random
from functools import total_ordering
from math import ceil
from typing import Optional, Callable

from .embedding import ScoredWord, Embedding
from .strategy import Strategy, Action


@total_ordering
class Turn:
    def __init__(self, action: Action, guess: Optional[ScoredWord] = None):
        self.action = action
        self.guess = guess

    def __le__(self, other: "Turn") -> bool:
        return self.guess <= other.guess

    def __str__(self):
        s = str(self.guess)
        if self.action is Action.HINT:
            s += f" (Hint)"
        return s


class QuitTurn(Turn):
    def __init__(self):
        super().__init__(Action.QUIT, None)

    def __str__(self):
        return "Quit"


class GuessHistory(list[Turn]):
    def __contains__(self, item) -> bool:
        if isinstance(item, ScoredWord):
            return any(item == turn.guess.word for turn in self)
        if isinstance(item, str):
            return any(item == turn.guess.word for turn in self)
        return super().__contains__(item)

    def __str__(self):
        return "\n".join(f"{i:,}. {item}" for i, item in enumerate(self, 1))


class Game:
    def __init__(
        self,
        embedding: Embedding,
        vocabulary_size: Optional[int],
        correct_word: Optional[str],
        observer: Callable[[Turn], None] = lambda _, __: _,
        hint_width: int = 500,
        hint_distance: int = 100,
    ):
        self.embedding = embedding
        self.vocabulary_size = vocabulary_size
        self.correct_word = correct_word or self.embedding.random_word(
            self.vocabulary_size
        )
        self.hint_width = hint_width
        self.hint_distance = hint_distance
        self.observer = observer

    def play(self, strategy: Strategy) -> tuple[bool, GuessHistory]:
        done = False
        history = GuessHistory()
        while not done:
            action, word = strategy(history)
            match action:
                case Action.GUESS:
                    scored_word = ScoredWord(word, self.embedding.similarity(word, self.correct_word))
                    turn = Turn(action, scored_word)
                    history.append(turn)
                    done = word == self.correct_word
                case Action.HINT:
                    scored_word = self.hint(history)
                    turn = Turn(action, scored_word)
                    history.append(turn)
                case Action.QUIT:
                    done = True
                    turn = QuitTurn()
                case _:
                    raise RuntimeError(f"Invalid action {action}")
            self.observer(turn)

        return history[-1].guess.word == self.correct_word, history

    def hint(self, history: GuessHistory) -> ScoredWord:
        while True:
            hints = self.embedding.guesses(
                [ScoredWord(self.correct_word, 1)],
                [],
                self.hint_distance + ceil(self.hint_width / 2),
                self.vocabulary_size,
            )[-self.hint_width:]
            hint = random.choice(hints)
            if hint.word not in history:
                return hint
