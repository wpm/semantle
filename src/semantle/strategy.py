from abc import abstractmethod
from enum import Enum, auto
from typing import Optional, TYPE_CHECKING

from .embedding import Embedding

if TYPE_CHECKING:
    from .game import GuessHistory


class Action(Enum):
    GUESS = auto()
    HINT = auto()
    QUIT = auto()


class Strategy:
    @abstractmethod
    def __call__(self, history: "GuessHistory") -> tuple[Action, Optional[str]]:
        return NotImplemented


class SemanticProximity(Strategy):
    def __init__(
        self,
        embedding: Embedding,
        similarity_threshold: float,
        suggestions_per_guess: int,
        vocabulary_size: Optional[int],
        initial_hints: int,
        remaining_guesses: int,
    ):
        self.embedding = embedding
        self.suggestions_per_guess = suggestions_per_guess
        self.similarity_threshold = similarity_threshold
        self.vocabulary_size = vocabulary_size
        self.initial_hints = initial_hints
        self.remaining_guesses = remaining_guesses

    def __call__(self, history: "GuessHistory") -> tuple[Action, Optional[str]]:
        action, word = None, None
        if self.remaining_guesses == 0:
            action = Action.QUIT
        elif self.initial_hints > 0:
            self.initial_hints -= 1
            action = Action.HINT
        else:
            action, word = Action.GUESS, self.guess(history)
        self.remaining_guesses -= 1
        return action, word

    def guess(self, history: "GuessHistory") -> str:
        if not history:
            return self.embedding.random_word(self.vocabulary_size)
        positive, negative = [], []
        for item in history:
            guess = item.guess
            if guess.score >= self.similarity_threshold:
                positive.append(guess)
            else:
                negative.append(guess)
        for guess in self.embedding.guesses(
            positive, negative, self.suggestions_per_guess, self.vocabulary_size
        ):
            if guess.word not in history:
                return guess.word
        return self.embedding.random_word(self.vocabulary_size)

    def __str__(self):
        return (
            "Semantic Proximity Strategy: "
            f"similarity threshold {self.similarity_threshold:0.4f}, "
            f"suggestions per guess {self.suggestions_per_guess}, "
            f"initial hints {self.initial_hints}"
        )
