"""
Machine assistant for the game Semantle
"""
import logging
from enum import Enum
from typing import Optional

import uvicorn
from tqdm import tqdm
from typer import Typer, Option, Exit

from . import __version__
from .embedding import (
    LocalEmbedding,
    DEFAULT_WORD2VEC_MODEL,
    RemoteEmbedding,
)
from .game import (
    GuessHistory,
    Game,
    Turn,
)
from .server import WORD2VEC_EMBEDDING_NAME
from .strategy import SemanticProximity, Action

app = Typer(help=__doc__, short_help="Semantle")


class LogLevel(str, Enum):
    DEBUG = "DEBUG"
    INFO = "INFO"
    WARNING = "WARNING"
    ERROR = "ERROR"
    CRITICAL = "CRITICAL"


def version_callback(value: bool):
    if value:
        print(__version__)
        raise Exit()


@app.callback()
def callback(
    _: bool = Option(None, "--version", callback=version_callback, is_eager=True),
    log_level: LogLevel = Option(
        LogLevel.WARNING, case_sensitive=False, show_default=True, help="logging level"
    ),
):
    # noinspection SpellCheckingInspection
    logging.basicConfig(
        format="%(levelname)s %(asctime)-15s %(message)s", level=log_level.value
    )


SUGGESTIONS_OPTION = Option(50, min=1, help="Number of suggestions per history prefix")
HISTORY_LENGTH_OPTION = Option(None, min=1, help="Maximum history length to consider")


SEMANTLE_API_PORT = "SEMANTLE_API_PORT"
DEFAULT_SEMANTLE_API_PORT = 5800

EMBEDDING_PORT_OPTION = Option(
    DEFAULT_SEMANTLE_API_PORT,
    help="Word embedding server port",
    envvar=SEMANTLE_API_PORT,
)


@app.command(
    name="simulator",
    short_help="simulate a game of Semantle",
)
def simulator_command(
    correct_word: str = Option(
        None, help="The word to be guessed [default: generate randomly]"
    ),
    num_suggestions: int = SUGGESTIONS_OPTION,
    similarity_threshold: float = Option(
        0.2, min=0, help="threshold at which to consider a term similar"
    ),
    vocabulary_size: Optional[int] = Option(None, help="Embedding vocabulary size"),
    embedding_host: str = Option(None, help="Word embedding server host"),
    embedding_port: int = EMBEDDING_PORT_OPTION,
    maximum_attempts: int = Option(1000, help="Maximum number of attempts"),
    hints: int = Option(0, min=0, help="Number of hints to receive"),
    hint_distance: int = Option(500, help="Hint distance from correct word"),
    hint_width: int = Option(100, help="Window size from which to draw hints"),
):
    """
    Simulate a Semantle game.
    """
    print("Semantle Simulator")
    if embedding_host:
        embedding = RemoteEmbedding(
            host=embedding_host,
            port=embedding_port,
            embedding_name=WORD2VEC_EMBEDDING_NAME,
        )
    else:
        embedding = LocalEmbedding(
            embedding_name=DEFAULT_WORD2VEC_MODEL,
        )
    correct_word = correct_word or embedding.random_word(vocabulary_size)
    logging.debug(f"Correct word: {correct_word}")
    strategy = SemanticProximity(
        embedding,
        similarity_threshold,
        num_suggestions,
        vocabulary_size,
        hints,
        maximum_attempts,
    )
    print(strategy)
    with tqdm(total=maximum_attempts) as progress:

        def observer(turn: Turn):

            match turn.action:
                case Action.GUESS | Action.HINT:
                    progress.set_description(f"{str(turn)[:30]:32}")
                    progress.update()

        game = Game(
            embedding,
            vocabulary_size,
            correct_word,
            observer,
            hint_width,
            hint_distance,
        )
        success, history = game.play(strategy)
    print(f"{history}\n")
    s = "Found" if success else "Did not find"
    print(f"{s} correct word: {correct_word}")
    # noinspection PyTypeChecker
    print(f"Closest guesses\n{GuessHistory(sorted(history)[:5])}")


@app.command(
    name="embeddings-server",
    short_help="launch word embeddings server",
)
def embeddings_server_command(embedding_port: int = EMBEDDING_PORT_OPTION):
    uvicorn.run(app="semantle.server:app", port=embedding_port)


def main():
    app()


if __name__ == "__main__":
    main()
