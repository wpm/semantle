import random
from abc import abstractmethod
from copy import copy
from dataclasses import dataclass
from functools import cached_property
from typing import Optional

import gensim.downloader as api
import requests
from cytoolz import take
from gensim.models import KeyedVectors

DEFAULT_WORD2VEC_MODEL = "word2vec-google-news-300"


@dataclass(frozen=True)
class ScoredWord:
    """
    A word associated with a score that sorts in descending score order.
    """

    word: str
    score: float

    def __le__(self, other: "ScoredWord") -> bool:
        # noinspection PyTypeChecker
        return (-self.score, self.word) <= (-other.score, other.word)

    @property
    def to_tuple(self) -> tuple[str, float]:
        return self.word, self.score

    @property
    def to_dict(self) -> dict[str, float]:
        return copy(self.__dict__)

    def __str__(self):
        return f"{self.word} {self.score:0.4f}"


class Embedding:
    """
    Interface to text embedding model that provides services used by Semantle.
    """

    @abstractmethod
    def similarity(self, a: str, b: str) -> float:
        return NotImplemented

    @abstractmethod
    def guesses(
        self,
        positive: list[ScoredWord],
        negative: list[ScoredWord],
        n: int,
        vocabulary_size: Optional[int],
    ) -> list[ScoredWord]:
        return NotImplemented

    @abstractmethod
    def random_word(self, vocabulary_size: Optional[int]) -> str:
        return NotImplemented


class LocalEmbedding(Embedding):
    """
    Interface to an embedding model hosted locally.
    """

    def __init__(self, embedding_name: str):
        self.embedding_name = embedding_name

    @cached_property
    def model(self) -> KeyedVectors:
        return api.load(self.embedding_name)

    def similarity(self, a: str, b: str) -> float:
        return self.model.similarity(a, b)

    def guesses(
        self,
        positive: list[ScoredWord],
        negative: list[ScoredWord],
        n: int,
        vocabulary_size: Optional[int],
    ) -> list[ScoredWord]:
        positive = [item.to_tuple for item in positive]
        negative = [item.word for item in negative]
        similar_items = self.model.most_similar(
            positive or None, negative or None, restrict_vocab=vocabulary_size
        )
        guesses = (
            ScoredWord(word, score)
            for word, score in similar_items
            if self.accept_word(word)
        )
        return list(take(n, guesses))

    def random_word(self, vocabulary_size: Optional[int]) -> str:
        k = self.model.index_to_key[:vocabulary_size]
        while True:
            word = random.choice(k)
            if self.accept_word(word):
                return word

    @staticmethod
    def accept_word(word: str) -> bool:
        return word.islower() and word.isalpha()


class RemoteEmbedding(Embedding):
    """
    Interface to an embedding model hosted via a REST API.
    """

    def __init__(
        self,
        host: str,
        port: int,
        embedding_name: str,
    ):
        self.url = f"{host}:{port}/{embedding_name}"

    def similarity(self, a: str, b: str) -> float:
        response = requests.post(f"{self.url}/similarity", params={"a": a, "b": b})
        response.raise_for_status()
        return response.json()

    def guesses(
        self,
        positive: list[ScoredWord],
        negative: list[ScoredWord],
        n: int,
        vocabulary_size: Optional[int],
    ) -> list[ScoredWord]:
        positive, negative = [
            [scored_word.to_dict for scored_word in t] for t in [positive, negative]
        ]
        response = requests.post(
            f"{self.url}/guesses",
            params={
                "n": n,
                "vocabulary_size": vocabulary_size,
            },
            json={
                "positive": positive,
                "negative": negative,
            },
        )
        response.raise_for_status()
        return [ScoredWord(**d) for d in response.json()]

    def random_word(self, vocabulary_size: Optional[int]) -> str:
        response = requests.get(
            f"{self.url}/random_word", params={"vocabulary_size": vocabulary_size}
        )
        response.raise_for_status()
        return response.json()
