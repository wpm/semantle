from typing import Optional

from fastapi import FastAPI, Depends

from . import __version__
from .embedding import ScoredWord, DEFAULT_WORD2VEC_MODEL, LocalEmbedding

app = FastAPI(
    title="Semantle Embeddings",
    description="Word Embeddings for the game Semantle",
    version=__version__,
)

WORD2VEC_EMBEDDING_NAME = "gensim"
word2vec_embedding_model = LocalEmbedding(DEFAULT_WORD2VEC_MODEL)


class Embeddings(dict[str, LocalEmbedding]):
    def __init__(self):
        super().__init__({WORD2VEC_EMBEDDING_NAME: word2vec_embedding_model})


@app.get("/")
def root():
    return {"API": "Semantle Embeddings", "version": __version__}


@app.post("/{embedding_name}/similarity")
def similarity(
    embedding_name: str,
    a: str,
    b: str,
    embeddings: Embeddings = Depends(),
) -> float:
    """
    The similarity score between two strings
    """
    return embeddings[embedding_name].similarity(a, b)


@app.post("/{embedding_name}/guesses")
def guesses(
    embedding_name: str,
    positive: list[dict[str, str | float]],
    negative: list[dict[str, str | float]],
    n: int,
    vocabulary_size: Optional[int] = None,
    embeddings: Embeddings = Depends(),
) -> list[ScoredWord]:
    """
    A set of guesses given positive and negative examples
    """
    positive, negative = [
        [ScoredWord(**scored_word) for scored_word in d] for d in [positive, negative]
    ]
    embedding = embeddings[embedding_name]
    return embedding.guesses(positive, negative, n, vocabulary_size)


@app.get("/{embedding_name}/random_word")
def random_word(
    embedding_name: str,
    vocabulary_size: Optional[int] = None,
    embeddings: Embeddings = Depends(),
) -> str:
    """
    A random word with an embedding in the model
    """
    return embeddings[embedding_name].random_word(vocabulary_size)
